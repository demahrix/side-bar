import 'package:flutter/widgets.dart';

/// Permet de departager les éléments de la sidebar en plusieurs groupe
class SideBarGroup {
  final String? label;
  final List<SideBarItem> items;
  const SideBarGroup({ this.label, required this.items });
}

class SideBarItem {
  final Widget icon;
  final String label;
  final int count;

  const SideBarItem({
    required this.icon,
    required this.label,
    this.count = 0
  });
}