
import 'dart:async';

class SideBarChangedData {
  final int? index;
  final int? groupIndex;
  const SideBarChangedData(this.index, { required this.groupIndex });
}

/// Permet de créer un bloc pret à l'emploi qui va permettre de recharger la side bar
class SideBarBloc {

  final SideBarChangedData _data;

  SideBarBloc([SideBarChangedData initialData = const SideBarChangedData(0, groupIndex: 0) ]): _data = initialData;

  final StreamController<SideBarChangedData> _controller = StreamController();
  Stream<SideBarChangedData> get listen => _controller.stream;
  SideBarChangedData get getData => _data;

  void change(int? index, int? groupIndex) {
    _controller.sink.add(SideBarChangedData(index, groupIndex: groupIndex));
  }

  void dispose() {
    _controller.close();
  }

}
