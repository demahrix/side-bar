import 'package:flutter/material.dart';
import 'side_bar_models.dart';

// FIXME retravailler sur ce widget widget
class SideBarItemWidget extends StatelessWidget {

  final IconThemeData iconTheme;
  final IconThemeData selectedIconTheme;
  final SideBarGroup group;
  final int groupIndex;
  final int itemIndex;
  final BottomNavigationBarThemeData theme;

  final SideBarItem item;
  final bool selected;

  final int? selectedGroupIndex;
  final int? selectedItemsIndex;

  final void Function(int groupIndex, int itemIndex) onTap;

  SideBarItemWidget({
    required this.iconTheme,
    required this.selectedIconTheme,
    required this.group,
    required this.groupIndex,
    required this.itemIndex,
    required this.theme,
    required this.selectedGroupIndex,
    required this.selectedItemsIndex,
    required this.onTap
  }): item = group.items[itemIndex],
      selected = groupIndex == selectedGroupIndex && itemIndex == selectedItemsIndex;

  @override
  Widget build(BuildContext context) {

    // final Color color = selected ? theme.selectedItemColor! : theme.unselectedItemColor!;

    final theme = selected ? selectedIconTheme : iconTheme;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: Material(
        color: selected ? Colors.grey.withOpacity(0.12) : Colors.transparent,
        borderRadius: const BorderRadius.all(Radius.circular(6.0)),
        clipBehavior: Clip.hardEdge,
        child: InkWell(
          onTap: () {
            if (!selected)
              onTap(groupIndex, itemIndex);
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            child: Row(
              children: [

                IconTheme(
                  data: theme,
                  child: item.icon,
                ),

                const SizedBox(width: 10.0),

                Text(
                  item.label,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: theme.color
                  ),
                ),

                if (item.count > 0)
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: _getNotifyCountWidget(item.count),
                    )
                  ),

              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _getNotifyCountWidget(int count) => Container(
    width: 20.0,
    height: 20.0,
    alignment: Alignment.center,
    decoration: const BoxDecoration(
      color: Colors.orange,
      shape: BoxShape.circle
    ),
    child: Text(
      count < 10 ? count.toString() : "9+",
      style: const TextStyle(
        fontSize: 11.0,
        color: Colors.white
      ),
    ),
  );

}
