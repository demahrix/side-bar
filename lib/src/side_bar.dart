import 'package:flutter/material.dart';
import 'package:side_bar/src/side_bar_item_widget.dart';
import 'side_bar_models.dart';

/// `NB`: Implementer BottomNavigationBarThemeData(backgroundColor, selectedItemColor, unselectedItemColor)
/// pour que ce widget fonctionne.
class SideBar extends StatelessWidget {

  final Widget? top;
  final Widget? bottom;
  final double width;
  final IconThemeData iconTheme;
  final IconThemeData selectedIconTheme;
  final List<SideBarGroup> groups;
  final int? selectedGroupIndex;
  final int? selectedItemsIndex;
  final void Function(int groupIndex, int itemIndex) onTap;
  // final SideBarController? controller;

  const SideBar({
    this.top,
    this.bottom,
    this.width = 220.0,
    required this.iconTheme,
    required this.selectedIconTheme,
    required this.groups,
    this.selectedGroupIndex = 0,
    this.selectedItemsIndex = 0,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final BottomNavigationBarThemeData navTheme = theme.bottomNavigationBarTheme;

    return Container(
      width: width,
      height: double.infinity,
      decoration: BoxDecoration(
        color: navTheme.backgroundColor,
        border: Border(
          right: BorderSide(width: 1.0, color: theme.brightness == Brightness.light ? Colors.black12 : Colors.white12)
        )
      ),
      child: Column(
        children: [
          if (top != null)
            top!,
          Expanded(
            // FIXME: faire un widget pour scroller
            child: SingleChildScrollView(
              child: Column(
                children: _buildItems(navTheme),
              ),
            ),
          ),
          if (bottom != null)
            bottom!
        ],
      ),
    );
  }

  List<Widget> _buildItems(BottomNavigationBarThemeData theme) {
    return List.generate(groups.length, (index) {
      final group = groups[index];
      return Column(
        children: [

          if (group.label != null)
          //index != 0 car on met pas d'espace en haut sur le premier element
            _getGroupLabelWidget(group.label!, index != 0),

          for (int i=0, len=group.items.length; i<len; ++i)
            SideBarItemWidget(
              iconTheme: iconTheme,
              selectedIconTheme: selectedIconTheme,
              group: group,
              groupIndex: index,
              itemIndex: i,
              theme: theme,
              selectedGroupIndex: selectedGroupIndex,
              selectedItemsIndex: selectedItemsIndex,
              onTap: onTap
            )

        ],
      );
    }, growable: false);
  }

  // Utiliser un themedata appropriete
  Widget _getGroupLabelWidget(String label, bool hasPaddingTop) {
    return Container(
      margin: EdgeInsets.only(top: hasPaddingTop ? 30.0 : 0.0, bottom: 10.0),
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      alignment: Alignment.bottomLeft,
      child: Text(
        label.toUpperCase(),
        style: const TextStyle(
          fontSize: 12.0,
          fontWeight: FontWeight.w600,
          color: Colors.grey
        ),
      ),
    );
  }

}